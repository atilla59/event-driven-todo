import {
  Column,
  CreateDateColumn,
  Entity,
  ObjectID,
  ObjectIdColumn,
} from 'typeorm';

@Entity()
export class Log {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  userId: string;

  @Column()
  todoId: string;

  @Column()
  type: string;

  @Column()
  action: string;

  @CreateDateColumn({ type: 'datetime' })
  createdAt: Date;
}
