import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';

import { LogsService } from './logs.service';
import { CreateLogDto } from './dto/create-log.dto';
import { Log } from './log.entity';

import { PaginationDto } from './dto/pagination.dto';

@Controller('logs')
export class LogsController {
  constructor(private readonly logsService: LogsService) {}

  @Get()
  @UsePipes(new ValidationPipe({ transform: true }))
  async findAll(@Query() paginateDto: PaginationDto) {
    return this.logsService.findAll(paginateDto);
  }

  @Post()
  async createUser(@Body() logDto: CreateLogDto): Promise<Log> {
    return await this.logsService.create(logDto);
  }
}
