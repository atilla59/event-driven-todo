import { Injectable, Logger } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';

import { getMongoManager, getMongoRepository, Repository } from 'typeorm';
import { Log } from './log.entity';
import { CreateLogDto, LogType } from './dto/create-log.dto';
import { PaginationDto } from './dto/pagination.dto';

@Injectable()
export class LogsService {
  constructor(@InjectRepository(Log) private logRepo: Repository<Log>) {}

  async create(dto: CreateLogDto | any) {
    return await this.logRepo.save(dto);
  }

  // async findOne(id: string) {
  //   return this.queryBus.execute(new FindUserQuery(id));
  // }
  //
  async findAll(paginateDto: PaginationDto) {
    Logger.log(JSON.stringify(paginateDto));

    const skip = paginateDto.page * paginateDto.perPage;

    const query = { where: {}, skip: skip, take: paginateDto.perPage };

    if (paginateDto.todoId) query.where['todoId'] = { $eq: paginateDto.todoId };
    if (paginateDto.userId) query.where['userId'] = { $eq: paginateDto.userId };
    if (paginateDto.actions)
      query.where['action'] = {
        $in: Array.isArray(paginateDto.actions)
          ? paginateDto.actions
          : [paginateDto.actions],
      };
    if (paginateDto.type)
      query.where['type'] = { $eq: LogType[paginateDto.type] };

    Logger.log(query);

    const res = await this.logRepo.findAndCount(query);

    return {
      data: res[0],
      total: res[1],
    };
  }
  //
  // async updateUser(id: string, dto: UpdateUserDto) {
  //   return this.commandBus.execute(new UpdateUserCommand(id, dto));
  // }
  //
  // async delete(id: string) {
  //   return this.commandBus.execute(new DeleteUserCommand(id));
  // }
}
