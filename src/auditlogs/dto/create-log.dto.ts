import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export enum LogType {
  'Command',
  'Event',
}

export class CreateLogDto {
  @IsOptional()
  @IsString()
  public id: string;

  @IsString()
  @IsNotEmpty()
  readonly userId: string;

  @IsString()
  @IsNotEmpty()
  readonly todoId: string;

  @IsEnum(LogType)
  @IsNotEmpty()
  readonly type: string;

  @IsNotEmpty()
  @IsString()
  readonly action: string;
}
