import {
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { LogType } from './create-log.dto';
import { Type } from 'class-transformer';

export class PaginationDto {
  @IsOptional()
  @IsString()
  userId: string;

  @IsOptional()
  @IsString()
  todoId: string;

  @IsEnum(LogType)
  @IsOptional()
  type: string;

  @IsOptional()
  @IsArray()
  events: [];

  @IsOptional()
  actions: [] | string;

  @IsOptional()
  @Type(() => Number)
  page = 0;

  @IsOptional()
  @Type(() => Number)
  perPage = 10;
}
