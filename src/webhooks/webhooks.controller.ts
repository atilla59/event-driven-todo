import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';

import { WebhooksService } from './webhooks.service';
import { CreateWebhookDto } from './dto/create-webhook.dto';
import { Webhook } from './webhook.entity';
import { UpdateUserDto } from '../users/dto/update-user.dto';
import { UpdateWebhookDto } from './dto/update-webhook.dto';

@Controller('webhooks')
export class WebhooksController {
  constructor(private readonly webhooksService: WebhooksService) {}

  @Get()
  async findAll() {
    return this.webhooksService.findEvents();
  }

  @Get(':id')
  async find(@Param('id') id: string) {
    return this.webhooksService.find(id);
  }

  @Post()
  async createWebhook(@Body() logDto: CreateWebhookDto): Promise<Webhook> {
    return await this.webhooksService.create(logDto);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() dto: UpdateWebhookDto) {
    return this.webhooksService.update(id, dto);
  }

  @Delete(':id')
  async deleteWebhook(@Param('id') id: string) {
    return this.webhooksService.delete(id);
  }
}
