import { Process, Processor } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';

@Processor('webhooks')
export class WebhooksProcessor {
  private readonly logger = new Logger(WebhooksProcessor.name);

  @Process('webhook')
  handle(job: Job) {
    this.logger.debug('Start webhook...');
    // HTTP logic write here
    this.logger.debug('Webhook completed');
  }
}
