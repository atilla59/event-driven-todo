import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';
import { Webhook } from './webhook.entity';
import { CreateWebhookDto } from './dto/create-webhook.dto';
import { UpdateWebhookDto } from './dto/update-webhook.dto';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@Injectable()
export class WebhooksService {
  constructor(
    @InjectRepository(Webhook) private webhookRepo: Repository<Webhook>,
    @InjectQueue('webhooks') private readonly webhooksQueue: Queue,
  ) {}

  async create(dto: CreateWebhookDto | any) {
    return await this.webhookRepo.save(dto);
  }

  async find(id: string) {
    return await this.webhookRepo.findOne(id);
  }

  findEvents() {
    return [
      'UserCreatedEvent',
      'UserDeletedEvent',
      'UserUpdatedEvent',
      'TodoCreatedEvent',
      'TodoDeletedEvent',
      'TodoUpdatedEvent',
      'TodoCompletedEvent',
    ];
  }

  async update(id: string, dto: UpdateWebhookDto) {
    const result = await this.webhookRepo.update(id, dto);
    return result.raw.result;
  }

  async delete(id: string) {
    const result = await this.webhookRepo.delete(id);
    return result.raw.deletedCount;
  }
}
