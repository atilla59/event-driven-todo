import {
  IsArray,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUrl,
} from 'class-validator';

export class UpdateWebhookDto {
  @IsString()
  @IsOptional()
  readonly userId?: string;

  @IsUrl()
  @IsOptional()
  readonly url?: string;

  @IsOptional()
  @IsArray()
  readonly events?: string[];
}
