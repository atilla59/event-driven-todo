import {
  IsArray,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUrl,
} from 'class-validator';

export class CreateWebhookDto {
  @IsOptional()
  @IsString()
  public id: string;

  @IsString()
  @IsNotEmpty()
  readonly userId: string;

  @IsUrl()
  @IsNotEmpty()
  readonly url: string;

  @IsNotEmpty()
  @IsArray()
  readonly events: string[];
}
