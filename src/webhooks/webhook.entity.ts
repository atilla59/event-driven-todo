import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';

@Entity()
export class Webhook {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  userId: string;

  @Column()
  url: string;

  @Column()
  events: string[];
}
