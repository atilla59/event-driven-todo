import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { UsersController } from './users.controller';

import { CommandHandlers } from './commands/handlers';
import { UsersService } from './users.service';
import { EventHandlers } from './events/handlers';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user.entity';
import { QueryHandlers } from './queries/handlers';

import { LogsModule } from '../auditlogs/logs.module';
import { BullModule } from '@nestjs/bull';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([User]),
    LogsModule,
    BullModule.registerQueue({
      name: 'webhooks',
    }),
  ],
  controllers: [UsersController],
  providers: [
    UsersService,
    ...CommandHandlers,
    ...EventHandlers,
    ...QueryHandlers,
  ],
})
export class UsersModule {}
