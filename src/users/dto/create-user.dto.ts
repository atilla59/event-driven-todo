import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateUserDto {
  @IsOptional()
  @IsString()
  public id: string;

  @IsString()
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  readonly email: string;
}
