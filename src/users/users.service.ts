import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateUserDto } from './dto/create-user.dto';
import { CreateUserCommand } from './commands/impl/create-user.command';
import { FindManyUsersQuery } from './queries/impl/find-many-users.query';
import { FindUserQuery } from './queries/impl/find-user.query';
import { UpdateUserDto } from './dto/update-user.dto';
import { UpdateUserCommand } from './commands/impl/update-user.command';
import { DeleteUserCommand } from './commands/impl/delete-user.command';

@Injectable()
export class UsersService {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  async createUser(dto: CreateUserDto) {
    return this.commandBus.execute(new CreateUserCommand(dto));
  }

  async findOne(id: string) {
    return this.queryBus.execute(new FindUserQuery(id));
  }

  async findAll() {
    return this.queryBus.execute(new FindManyUsersQuery());
  }

  async updateUser(id: string, dto: UpdateUserDto) {
    return this.commandBus.execute(new UpdateUserCommand(id, dto));
  }

  async delete(id: string) {
    return this.commandBus.execute(new DeleteUserCommand(id));
  }
}
