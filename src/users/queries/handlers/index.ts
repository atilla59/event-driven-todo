import { FindManyUsersHandlers } from './find-many-users.handlers';
import { FindUserHandlers } from './find-user.handlers';

export const QueryHandlers = [FindManyUsersHandlers, FindUserHandlers];
