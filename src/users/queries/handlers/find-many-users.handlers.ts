import { Logger } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { FindManyUsersQuery } from '../impl/find-many-users.query';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../user.entity';
import { Repository } from 'typeorm';

@QueryHandler(FindManyUsersQuery)
export class FindManyUsersHandlers
  implements IQueryHandler<FindManyUsersQuery>
{
  constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

  async execute(query: FindManyUsersQuery) {
    Logger.log('Async FindManyUsersHandlers...', 'FindManyUsersQuery');

    Logger.log(JSON.stringify(query));
    return this.userRepo.find({ relations: ['todos'] });
  }
}
