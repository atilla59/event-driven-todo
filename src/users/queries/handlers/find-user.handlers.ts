import { Logger } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { FindUserQuery } from '../impl/find-user.query';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../user.entity';
import { Repository } from 'typeorm';

@QueryHandler(FindUserQuery)
export class FindUserHandlers implements IQueryHandler<FindUserQuery> {
  constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

  async execute(query: FindUserQuery) {
    Logger.log('Async FindUserQueryHandlers...', 'FindUserQuery');

    return await this.userRepo.findOne(query.id, { relations: ['todos'] });
  }
}
