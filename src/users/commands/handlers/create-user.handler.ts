import { Inject, Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { CreateUserCommand } from 'src/users/commands/impl/create-user.command';

import { UserRoot } from '../../users.model';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../user.entity';
import { Repository } from 'typeorm';
import { LogsService } from '../../../auditlogs/logs.service';
import { LogType } from '../../../auditlogs/dto/create-log.dto';

@CommandHandler(CreateUserCommand)
export class CreateUserHandler implements ICommandHandler<CreateUserCommand> {
  constructor(
    @InjectRepository(User) private userRepo: Repository<User>,
    private logsService: LogsService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: CreateUserCommand) {
    Logger.log('Async CreateUserHandler..., "CreateUserCommand"');

    const { userDto } = command;

    const userCreated = await this.userRepo.save(userDto);

    await this.logsService.create({
      userId: userDto.id,
      type: LogType.Command,
      action: 'CreateUserCommand',
    });

    const userRoot = new UserRoot(userCreated.id);

    const user = this.publisher.mergeObjectContext(userRoot);
    user.createUser(userCreated);
    user.commit();
  }
}
