import { Inject, Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { CreateUserCommand } from 'src/users/commands/impl/create-user.command';

import { UserRoot } from '../../users.model';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../user.entity';
import { Repository } from 'typeorm';
import { UpdateUserCommand } from '../impl/update-user.command';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { LogsService } from '../../../auditlogs/logs.service';

@CommandHandler(UpdateUserCommand)
export class UpdateUserHandler implements ICommandHandler<UpdateUserCommand> {
  constructor(
    @InjectRepository(User) private userRepo: Repository<User>,
    private logsService: LogsService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: UpdateUserCommand) {
    Logger.log('Async UpdateUserHandler..., "UpdateUserCommand"', Date.now());

    const { id, updateUserDto } = command;

    const userUpdated = await this.userRepo.update(id, updateUserDto);

    await this.logsService.create({
      userId: id,
      type: LogType.Command,
      action: 'UpdateUserCommand',
    });

    const userRoot = new UserRoot(id);
    userRoot.setData(await this.userRepo.findOne(id));

    const user = this.publisher.mergeObjectContext(userRoot);
    user.updateUser();
    user.commit();
  }
}
