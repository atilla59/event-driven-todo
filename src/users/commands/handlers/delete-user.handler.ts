import { Inject, Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { CreateUserCommand } from 'src/users/commands/impl/create-user.command';

import { UserRoot } from '../../users.model';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../../user.entity';
import { Repository } from 'typeorm';
import { UpdateUserCommand } from '../impl/update-user.command';
import { DeleteUserCommand } from '../impl/delete-user.command';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { LogsService } from '../../../auditlogs/logs.service';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@CommandHandler(DeleteUserCommand)
export class DeleteUserHandler implements ICommandHandler<DeleteUserCommand> {
  constructor(
    @InjectRepository(User) private userRepo: Repository<User>,

    private logsService: LogsService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: DeleteUserCommand) {
    Logger.log('Async DeleteUserHandler..., "DeleteUserCommand"');

    const { id } = command;

    const userDeleted = await this.userRepo.delete(id);

    await this.logsService.create({
      userId: id,
      type: LogType.Command,
      action: 'DeleteUserCommand',
    });

    Logger.log('DeleteUserCommand', JSON.stringify(userDeleted.affected));

    const userRoot = new UserRoot(id);

    const user = this.publisher.mergeObjectContext(userRoot);
    user.deleteUser();
    user.commit();
  }
}
