import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { ICommand } from '@nestjs/cqrs';
import { UpdateUserDto } from '../../dto/update-user.dto';

export class DeleteUserCommand implements ICommand {
  constructor(public readonly id: string) {}
}
