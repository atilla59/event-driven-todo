import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { ICommand } from '@nestjs/cqrs';

export class CreateUserCommand implements ICommand {
  constructor(public readonly userDto: CreateUserDto) {}
}
