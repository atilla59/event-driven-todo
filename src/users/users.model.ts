import { AggregateRoot } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';

import { User } from './user.entity';
import { ObjectID } from 'typeorm';
import { UserCreatedEvent } from './events/impl/user-created.event';
import { CreateUserDto } from './dto/create-user.dto';
import { UserUpdatedEvent } from './events/impl/user-updated.event';
import { UserDeletedEvent } from './events/impl/user-deleted.event';

export class UserRoot extends AggregateRoot {
  data: User;

  constructor(private id: string) {
    super();
  }

  setData(data: User) {
    this.data = data;
  }

  createUser(dto: CreateUserDto) {
    Logger.log('createUser AggregateRoot');
    this.apply(new UserCreatedEvent(dto));
  }

  updateUser() {
    Logger.log('updateUser AggregateRoot');
    this.apply(new UserUpdatedEvent(this.data));
  }

  deleteUser() {
    Logger.log('deleteUser AggregateRoot');
    this.apply(new UserDeletedEvent(this.id));
  }
}
