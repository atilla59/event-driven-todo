import { IEvent } from '@nestjs/cqrs';
import { UpdateUserDto } from '../../dto/update-user.dto';

export class UserUpdatedEvent implements IEvent {
  constructor(public readonly userDto: any) {}
}
