import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { UserCreatedEvent } from '../impl/user-created.event';
import { LogsService } from '../../../auditlogs/logs.service';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@EventsHandler(UserCreatedEvent)
export class UserCreatedHandler implements IEventHandler<UserCreatedEvent> {
  constructor(
    private logsService: LogsService,
    @InjectQueue('webhooks') private readonly webhooksQueue: Queue,
  ) {}

  async handle(event: UserCreatedEvent) {
    await this.logsService.create({
      userId: event.userDto.id,
      type: LogType.Event,
      action: 'UserCreatedEvent',
    });

    // add webhook queue
    await this.webhooksQueue.add('webhook', {
      event: 'UserCreatedEvent',
    });

    Logger.log(JSON.stringify(event), ' UserCreatedEvent');
  }
}
