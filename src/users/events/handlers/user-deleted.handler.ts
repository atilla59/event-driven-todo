import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { UserCreatedEvent } from '../impl/user-created.event';
import { UserUpdatedEvent } from '../impl/user-updated.event';
import { UserDeletedEvent } from '../impl/user-deleted.event';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { LogsService } from '../../../auditlogs/logs.service';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@EventsHandler(UserDeletedEvent)
export class UserDeletedHandler implements IEventHandler<UserDeletedEvent> {
  constructor(
    private logsService: LogsService,
    @InjectQueue('webhooks') private readonly webhooksQueue: Queue,
  ) {}

  async handle(event: UserDeletedEvent) {
    await this.logsService.create({
      userId: event.id,
      type: LogType.Event,
      action: 'UserDeletedEvent',
    });

    // add webhook queue
    await this.webhooksQueue.add('webhook', {
      event: 'UserDeletedEvent',
    });

    Logger.log(JSON.stringify(event), ' UserDeletedEvent');
  }
}
