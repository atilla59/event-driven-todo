import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { UserCreatedEvent } from '../impl/user-created.event';
import { UserUpdatedEvent } from '../impl/user-updated.event';
import { LogsService } from '../../../auditlogs/logs.service';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@EventsHandler(UserUpdatedEvent)
export class UserUpdatedHandler implements IEventHandler<UserUpdatedEvent> {
  constructor(
    private logsService: LogsService,
    @InjectQueue('webhooks') private readonly webhooksQueue: Queue,
  ) {}

  async handle(event: UserUpdatedEvent) {
    await this.logsService.create({
      userId: event.userDto.id,
      type: LogType.Event,
      action: 'UserUpdatedEvent',
    });

    // add webhook queue
    await this.webhooksQueue.add('webhook', {
      event: 'UserUpdatedEvent',
    });
    Logger.log(JSON.stringify(event), ' UserUpdatedEvent', Date.now());
  }
}
