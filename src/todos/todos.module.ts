import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TodosController } from './todos.controller';

import { CommandHandlers } from './commands/handlers';
import { EventHandlers } from './events/handlers';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Todo } from './todo.entity';
import { QueryHandlers } from './queries/handlers';

import { LogsModule } from '../auditlogs/logs.module';
import { BullModule } from '@nestjs/bull';
import { TodosService } from './todos.service';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([Todo]),
    LogsModule,
    BullModule.registerQueue({
      name: 'webhooks',
    }),
  ],
  controllers: [TodosController],
  providers: [
    TodosService,
    ...CommandHandlers,
    ...EventHandlers,
    ...QueryHandlers,
  ],
})
export class TodosModule {}
