import { ICommand } from '@nestjs/cqrs';
import { AssignTodoDto } from '../../dto/assign-todo.dto';

export class AssignTodoCommand implements ICommand {
  constructor(public readonly assignDto: AssignTodoDto) {}
}
