import { ICommand } from '@nestjs/cqrs';
import { UpdateTodoDto } from '../../dto/update-todo.dto';

export class UpdateTodoCommand implements ICommand {
  constructor(
    public readonly id: string,
    public readonly updateTodoDto: UpdateTodoDto,
  ) {}
}
