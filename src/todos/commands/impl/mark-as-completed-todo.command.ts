import { ICommand } from '@nestjs/cqrs';
import { AssignTodoDto } from '../../dto/assign-todo.dto';

export class MarkAsCompletedTodoCommand implements ICommand {
  constructor(public readonly id: string) {}
}
