import { Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TodoRoot } from '../../todos.model';
import { InjectRepository } from '@nestjs/typeorm';
import { Todo } from '../../todo.entity';
import { Repository } from 'typeorm';
import { UpdateTodoCommand } from '../impl/update-todo.command';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { LogsService } from '../../../auditlogs/logs.service';
import { AssignTodoCommand } from '../impl/assign-todo.command';

@CommandHandler(AssignTodoCommand)
export class AssignTodoHandler implements ICommandHandler<AssignTodoCommand> {
  constructor(
    @InjectRepository(Todo) private todoRepo: Repository<Todo>,
    private logsService: LogsService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: AssignTodoCommand) {
    Logger.log('Async AssignTodoHandler..., "AssignTodoCommand"');

    const { assignDto } = command;

    const todoUpdated = await this.todoRepo.update(assignDto.id, {
      userId: assignDto.userId,
    });

    await this.logsService.create({
      todoId: assignDto.id,
      userId: assignDto.userId,
      type: LogType.Command,
      action: 'AssignTodoCommand',
    });

    const todoRoot = new TodoRoot(assignDto.id);
    todoRoot.setData(await this.todoRepo.findOne(assignDto.id));

    const todo = this.publisher.mergeObjectContext(todoRoot);
    todo.assignTodo();
    todo.commit();
  }
}
