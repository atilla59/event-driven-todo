import { CreateTodoHandler } from './create-todo.handler';
import { UpdateTodoHandler } from './update-todo.handler';
import { DeleteTodoHandler } from './delete-todo.handler';
import { AssignTodoHandler } from './assign-todo.handler';
import { MarkAsCompletedTodoHandler } from './mark-as-completed-todo.handler';

export const CommandHandlers = [
  CreateTodoHandler,
  UpdateTodoHandler,
  DeleteTodoHandler,
  AssignTodoHandler,
  MarkAsCompletedTodoHandler,
];
