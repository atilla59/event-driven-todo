import { Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TodoRoot } from '../../todos.model';
import { InjectRepository } from '@nestjs/typeorm';
import { Todo } from '../../todo.entity';
import { Repository } from 'typeorm';
import { UpdateTodoCommand } from '../impl/update-todo.command';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { LogsService } from '../../../auditlogs/logs.service';
import { AssignTodoCommand } from '../impl/assign-todo.command';
import { MarkAsCompletedTodoCommand } from '../impl/mark-as-completed-todo.command';

@CommandHandler(MarkAsCompletedTodoCommand)
export class MarkAsCompletedTodoHandler
  implements ICommandHandler<MarkAsCompletedTodoCommand>
{
  constructor(
    @InjectRepository(Todo) private todoRepo: Repository<Todo>,
    private logsService: LogsService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: MarkAsCompletedTodoCommand) {
    Logger.log(
      'Async MarkAsCompletedTodoHandler..., "MarkAsCompletedTodoCommand"',
    );

    const { id } = command;

    const todoUpdated = await this.todoRepo.update(id, {
      isCompleted: true,
    });

    await this.logsService.create({
      todoId: id,
      type: LogType.Command,
      action: 'MarkAsCompletedTodoCommand',
    });

    const todoRoot = new TodoRoot(id);
    todoRoot.setData(await this.todoRepo.findOne(id));

    const todo = this.publisher.mergeObjectContext(todoRoot);
    todo.markAsCompleted();
    todo.commit();
  }
}
