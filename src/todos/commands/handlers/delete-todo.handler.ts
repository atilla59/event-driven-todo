import { Inject, Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';

import { DeleteTodoCommand } from '../impl/delete-todo.command';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { LogsService } from '../../../auditlogs/logs.service';
import { Todo } from '../../todo.entity';
import { TodoRoot } from '../../todos.model';

@CommandHandler(DeleteTodoCommand)
export class DeleteTodoHandler implements ICommandHandler<DeleteTodoCommand> {
  constructor(
    @InjectRepository(Todo) private todoRepo: Repository<Todo>,

    private logsService: LogsService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: DeleteTodoCommand) {
    Logger.log('Async DeleteTodoHandler..., "DeleteTodoCommand"');

    const { id } = command;

    const todoDeleted = await this.todoRepo.delete(id);

    await this.logsService.create({
      todoId: id,
      type: LogType.Command,
      action: 'DeleteTodoCommand',
    });

    Logger.log('DeleteTodoCommand', JSON.stringify(todoDeleted.affected));

    const todoRoot = new TodoRoot(id);

    const todo = this.publisher.mergeObjectContext(todoRoot);
    todo.deleteTodo();
    todo.commit();
  }
}
