import { Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TodoRoot } from '../../todos.model';
import { InjectRepository } from '@nestjs/typeorm';
import { Todo } from '../../todo.entity';
import { Repository } from 'typeorm';
import { UpdateTodoCommand } from '../impl/update-todo.command';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { LogsService } from '../../../auditlogs/logs.service';

@CommandHandler(UpdateTodoCommand)
export class UpdateTodoHandler implements ICommandHandler<UpdateTodoCommand> {
  constructor(
    @InjectRepository(Todo) private todoRepo: Repository<Todo>,
    private logsService: LogsService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: UpdateTodoCommand) {
    Logger.log('Async UpdateTodoHandler..., "UpdateTodoCommand"');

    const { id, updateTodoDto } = command;

    const todoUpdated = await this.todoRepo.update(id, updateTodoDto);

    await this.logsService.create({
      todoId: id,
      type: LogType.Command,
      action: 'UpdateTodoCommand',
    });

    const todoRoot = new TodoRoot(id);
    todoRoot.setData(await this.todoRepo.findOne(id));

    const todo = this.publisher.mergeObjectContext(todoRoot);
    todo.updateTodo();
    todo.commit();
  }
}
