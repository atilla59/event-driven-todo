import { Inject, Logger } from '@nestjs/common';
import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';

import { TodoRoot } from '../../todos.model';
import { InjectRepository } from '@nestjs/typeorm';
import { Todo } from '../../todo.entity';
import { Repository } from 'typeorm';
import { LogsService } from '../../../auditlogs/logs.service';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { CreateTodoCommand } from '../impl/create-todo.command';

@CommandHandler(CreateTodoCommand)
export class CreateTodoHandler implements ICommandHandler<CreateTodoCommand> {
  constructor(
    @InjectRepository(Todo) private todoRepo: Repository<Todo>,
    private logsService: LogsService,
    private readonly publisher: EventPublisher,
  ) {}

  async execute(command: CreateTodoCommand) {
    Logger.log('Async CreateTodoHandler..., "CreateTodoCommand"');

    const { todoDto } = command;

    const todoCreated = await this.todoRepo.save(todoDto);

    await this.logsService.create({
      todoId: todoDto.id,
      type: LogType.Command,
      action: 'CreateTodoCommand',
    });

    const todoRoot = new TodoRoot(todoCreated.id);

    const todo = this.publisher.mergeObjectContext(todoRoot);
    todo.createTodo(todoCreated);
    todo.commit();
  }
}
