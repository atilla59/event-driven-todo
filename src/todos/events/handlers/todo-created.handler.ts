import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TodoCreatedEvent } from '../impl/todo-created.event';
import { LogsService } from '../../../auditlogs/logs.service';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@EventsHandler(TodoCreatedEvent)
export class TodoCreatedHandler implements IEventHandler<TodoCreatedEvent> {
  constructor(
    private logsService: LogsService,
    @InjectQueue('webhooks') private readonly webhooksQueue: Queue,
  ) {}

  async handle(event: TodoCreatedEvent) {
    await this.logsService.create({
      todoId: event.todoDto.id,
      type: LogType.Event,
      action: 'TodoCreatedEvent',
    });

    // add webhook queue
    await this.webhooksQueue.add('webhook', {
      event: 'TodoCreatedEvent',
    });

    Logger.log(JSON.stringify(event), ' TodoCreatedEvent');
  }
}
