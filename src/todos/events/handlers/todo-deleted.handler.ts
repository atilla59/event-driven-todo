import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TodoDeletedEvent } from '../impl/todo-deleted.event';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { LogsService } from '../../../auditlogs/logs.service';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@EventsHandler(TodoDeletedEvent)
export class TodoDeletedHandler implements IEventHandler<TodoDeletedEvent> {
  constructor(
    private logsService: LogsService,
    @InjectQueue('webhooks') private readonly webhooksQueue: Queue,
  ) {}

  async handle(event: TodoDeletedEvent) {
    await this.logsService.create({
      todoId: event.id,
      type: LogType.Event,
      action: 'TodoDeletedEvent',
    });

    // add webhook queue
    await this.webhooksQueue.add('webhook', {
      event: 'TodoDeletedEvent',
    });

    Logger.log(JSON.stringify(event), ' TodoDeletedEvent');
  }
}
