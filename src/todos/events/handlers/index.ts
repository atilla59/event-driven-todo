import { TodoCreatedHandler } from './todo-created.handler';
import { TodoDeletedHandler } from './todo-deleted.handler';
import { TodoUpdatedHandler } from './todo-updated.handler';
import { TodoAssignHandler } from './todo-assign.handler';
import { TodoMarkAsCompletedHandler } from './todo-mark-as-completed.handler';

export const EventHandlers = [
  TodoCreatedHandler,
  TodoDeletedHandler,
  TodoUpdatedHandler,
  TodoAssignHandler,
  TodoMarkAsCompletedHandler,
];
