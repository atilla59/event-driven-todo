import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TodoCreatedEvent } from '../impl/todo-created.event';
import { TodoUpdatedEvent } from '../impl/todo-updated.event';
import { LogsService } from '../../../auditlogs/logs.service';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { TodoAssignEvent } from '../impl/todo-assign.event';
import { TodoMarkAsCompletedEvent } from '../impl/todo-mark-as-completed.event';

@EventsHandler(TodoMarkAsCompletedEvent)
export class TodoMarkAsCompletedHandler
  implements IEventHandler<TodoMarkAsCompletedEvent>
{
  constructor(
    private logsService: LogsService,
    @InjectQueue('webhooks') private readonly webhooksQueue: Queue,
  ) {}

  async handle(event: TodoMarkAsCompletedEvent) {
    await this.logsService.create({
      todoId: event.id,
      type: LogType.Event,
      action: 'TodoMarkAsCompletedEvent',
    });

    // add webhook queue
    await this.webhooksQueue.add('webhook', {
      event: 'TodoMarkAsCompletedEvent',
    });
    Logger.log(JSON.stringify(event), ' TodoMarkAsCompletedEvent');
  }
}
