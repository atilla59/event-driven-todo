import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TodoCreatedEvent } from '../impl/todo-created.event';
import { TodoUpdatedEvent } from '../impl/todo-updated.event';
import { LogsService } from '../../../auditlogs/logs.service';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { TodoAssignEvent } from '../impl/todo-assign.event';

@EventsHandler(TodoAssignEvent)
export class TodoAssignHandler implements IEventHandler<TodoAssignEvent> {
  constructor(
    private logsService: LogsService,
    @InjectQueue('webhooks') private readonly webhooksQueue: Queue,
  ) {}

  async handle(event: TodoAssignEvent) {
    await this.logsService.create({
      todoId: event.assignDto.id,
      type: LogType.Event,
      action: 'TodoAssignedEvent',
    });

    // add webhook queue
    await this.webhooksQueue.add('webhook', {
      event: 'TodoAssignedEvent',
    });
    Logger.log(JSON.stringify(event), ' TodoAssignedEvent');
  }
}
