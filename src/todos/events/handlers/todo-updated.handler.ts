import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';
import { TodoCreatedEvent } from '../impl/todo-created.event';
import { TodoUpdatedEvent } from '../impl/todo-updated.event';
import { LogsService } from '../../../auditlogs/logs.service';
import { LogType } from '../../../auditlogs/dto/create-log.dto';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@EventsHandler(TodoUpdatedEvent)
export class TodoUpdatedHandler implements IEventHandler<TodoUpdatedEvent> {
  constructor(
    private logsService: LogsService,
    @InjectQueue('webhooks') private readonly webhooksQueue: Queue,
  ) {}

  async handle(event: TodoUpdatedEvent) {
    await this.logsService.create({
      todoId: event.todoDto.id,
      type: LogType.Event,
      action: 'TodoUpdatedEvent',
    });

    // add webhook queue
    await this.webhooksQueue.add('webhook', {
      event: 'TodoUpdatedEvent',
    });
    Logger.log(JSON.stringify(event), ' TodoUpdatedEvent');
  }
}
