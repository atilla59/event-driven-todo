import { IEvent } from '@nestjs/cqrs';

export class TodoDeletedEvent implements IEvent {
  constructor(public readonly id: string) {}
}
