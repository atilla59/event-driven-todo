import { IEvent } from '@nestjs/cqrs';
import { UpdateTodoDto } from '../../dto/update-todo.dto';

export class TodoUpdatedEvent implements IEvent {
  constructor(public readonly todoDto: any) {}
}
