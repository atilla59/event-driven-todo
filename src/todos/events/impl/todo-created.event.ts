import { IEvent } from '@nestjs/cqrs';
import { CreateTodoDto } from '../../dto/create-todo.dto';

export class TodoCreatedEvent implements IEvent {
  constructor(public readonly todoDto: CreateTodoDto) {}
}
