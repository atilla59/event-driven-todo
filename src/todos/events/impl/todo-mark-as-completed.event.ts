import { IEvent } from '@nestjs/cqrs';

export class TodoMarkAsCompletedEvent implements IEvent {
  constructor(public readonly id: string) {}
}
