import { IEvent } from '@nestjs/cqrs';

export class TodoAssignEvent implements IEvent {
  constructor(public readonly assignDto: any) {}
}
