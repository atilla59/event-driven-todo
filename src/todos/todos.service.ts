import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateTodoDto } from './dto/create-todo.dto';
import { CreateTodoCommand } from './commands/impl/create-todo.command';
import { FindManyTodosQuery } from './queries/impl/find-many-todos.query';
import { FindTodoQuery } from './queries/impl/find-todo.query';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { UpdateTodoCommand } from './commands/impl/update-todo.command';
import { DeleteTodoCommand } from './commands/impl/delete-todo.command';
import { AssignTodoDto } from './dto/assign-todo.dto';
import { AssignTodoCommand } from './commands/impl/assign-todo.command';
import { MarkAsCompletedTodoCommand } from './commands/impl/mark-as-completed-todo.command';

@Injectable()
export class TodosService {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  async createTodo(dto: CreateTodoDto) {
    return this.commandBus.execute(new CreateTodoCommand(dto));
  }

  async findOne(id: string) {
    return this.queryBus.execute(new FindTodoQuery(id));
  }

  async findAll() {
    return this.queryBus.execute(new FindManyTodosQuery());
  }

  async updateTodo(id: string, dto: UpdateTodoDto) {
    return this.commandBus.execute(new UpdateTodoCommand(id, dto));
  }

  async delete(id: string) {
    return this.commandBus.execute(new DeleteTodoCommand(id));
  }

  async assignUser(assignDto: AssignTodoDto) {
    return this.commandBus.execute(new AssignTodoCommand(assignDto));
  }

  async markAsCompleted(id: string) {
    return this.commandBus.execute(new MarkAsCompletedTodoCommand(id));
  }
}
