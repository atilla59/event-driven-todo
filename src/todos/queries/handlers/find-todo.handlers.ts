import { Logger } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { FindTodoQuery } from '../impl/find-todo.query';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';
import { Todo } from '../../todo.entity';

@QueryHandler(FindTodoQuery)
export class FindTodoHandlers implements IQueryHandler<FindTodoQuery> {
  constructor(@InjectRepository(Todo) private todoRepo: Repository<Todo>) {}

  async execute(query: FindTodoQuery) {
    Logger.log('Async FindTodoQueryHandlers...', 'FindTodoQuery');

    return this.todoRepo.findOne(query.id);
  }
}
