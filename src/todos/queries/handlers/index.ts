import { FindManyTodosHandlers } from './find-many-todos.handlers';
import { FindTodoHandlers } from './find-todo.handlers';

export const QueryHandlers = [FindManyTodosHandlers, FindTodoHandlers];
