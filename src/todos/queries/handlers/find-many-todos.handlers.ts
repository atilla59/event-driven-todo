import { Logger } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { FindManyTodosQuery } from '../impl/find-many-todos.query';
import { InjectRepository } from '@nestjs/typeorm';
import { Todo } from '../../todo.entity';
import { Repository } from 'typeorm';

@QueryHandler(FindManyTodosQuery)
export class FindManyTodosHandlers
  implements IQueryHandler<FindManyTodosQuery>
{
  constructor(@InjectRepository(Todo) private todoRepo: Repository<Todo>) {}

  async execute(query: FindManyTodosQuery) {
    Logger.log('Async FindManyTodosHandlers...', 'FindManyTodosQuery');

    Logger.log(JSON.stringify(query));
    return this.todoRepo.find();
  }
}
