import { IQuery } from '@nestjs/cqrs';

export class FindTodoQuery implements IQuery {
  constructor(public readonly id: string) {}
}
