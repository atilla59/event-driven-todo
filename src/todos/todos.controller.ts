import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { Todo } from './todo.entity';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { TodosService } from './todos.service';
import { AssignTodoDto } from './dto/assign-todo.dto';

@Controller('todo')
export class TodosController {
  constructor(private readonly todosService: TodosService) {}

  @Get()
  async findAll() {
    return this.todosService.findAll();
  }

  @Get(':id')
  async find(@Param('id') id: string) {
    return this.todosService.findOne(id);
  }

  @Put(':id')
  async update(@Param('id') id: string, @Body() todoDto: UpdateTodoDto) {
    return this.todosService.updateTodo(id, todoDto);
  }

  @Put(':id/assign-user/:userId')
  async assignUser(@Param() assignDto: AssignTodoDto) {
    return this.todosService.assignUser(assignDto);
  }

  @Put(':id/mark-as-completed')
  async markAsCompleted(@Param('id') id: string) {
    return this.todosService.markAsCompleted(id);
  }

  @Post()
  async createTodo(@Body() todoDto: CreateTodoDto): Promise<Todo> {
    return this.todosService.createTodo(todoDto);
  }

  @Delete(':id')
  async deleteTodo(@Param('id') id: string) {
    return this.todosService.delete(id);
  }
}
