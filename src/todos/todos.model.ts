import { AggregateRoot } from '@nestjs/cqrs';
import { Logger } from '@nestjs/common';

import { Todo } from './todo.entity';
import { ObjectID } from 'typeorm';
import { TodoCreatedEvent } from './events/impl/todo-created.event';
import { CreateTodoDto } from './dto/create-todo.dto';
import { TodoUpdatedEvent } from './events/impl/todo-updated.event';
import { TodoDeletedEvent } from './events/impl/todo-deleted.event';
import { TodoAssignEvent } from './events/impl/todo-assign.event';
import { TodoMarkAsCompletedEvent } from './events/impl/todo-mark-as-completed.event';

export class TodoRoot extends AggregateRoot {
  data: Todo;

  constructor(private id: string) {
    super();
  }

  setData(data: Todo) {
    this.data = data;
  }

  createTodo(dto: CreateTodoDto) {
    Logger.log('createTodo AggregateRoot');
    this.apply(new TodoCreatedEvent(dto));
  }

  updateTodo() {
    Logger.log('updateTodo AggregateRoot');
    this.apply(new TodoUpdatedEvent(this.data));
  }

  deleteTodo() {
    Logger.log('deleteTodo AggregateRoot');
    this.apply(new TodoDeletedEvent(this.id));
  }

  assignTodo() {
    Logger.log('assignTodo AggregateRoot');
    this.apply(new TodoAssignEvent(this.id));
  }

  markAsCompleted() {
    Logger.log('markAsCompleted AggregateRoot');
    this.apply(new TodoMarkAsCompletedEvent(this.id));
  }
}
