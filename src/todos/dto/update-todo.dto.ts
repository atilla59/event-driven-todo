import {
  IsBoolean,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateTodoDto {
  @IsOptional()
  @IsString()
  public id?: string;

  @IsString()
  @IsOptional()
  readonly name?: string;

  @IsEmail()
  @IsOptional()
  readonly description?: string;

  @IsString()
  @IsOptional()
  readonly userId?: string;

  @IsBoolean()
  @IsOptional()
  readonly isCompleted?: boolean = false;
}
