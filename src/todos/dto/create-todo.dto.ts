import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateTodoDto {
  @IsOptional()
  @IsString()
  public id: string;

  @IsString()
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  readonly description: string;

  @IsString()
  @IsOptional()
  readonly userId: string;

  @IsBoolean()
  @IsOptional()
  readonly isCompleted: boolean = false;
}
