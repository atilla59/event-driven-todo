import { IsNotEmpty, IsString } from 'class-validator';

export class AssignTodoDto {
  @IsNotEmpty()
  @IsString()
  public id: string;

  @IsString()
  @IsNotEmpty()
  readonly userId: string;
}
