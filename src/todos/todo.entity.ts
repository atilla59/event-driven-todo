import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToOne,
  ObjectID,
  ObjectIdColumn,
} from 'typeorm';
import { User } from '../users/user.entity';

@Entity()
export class Todo {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column('boolean', { default: false })
  isCompleted: boolean;

  @Column()
  userId: string;

  @ManyToOne(() => User, (user) => user.todos)
  user: User;
}
