# Event Driven Todo

This application written in [NestJs](https://nestjs.com/)
Framework

## Installation

```bash
$ npm install
```

## Running the app

```bash

# for docker development
$ docker-compose up

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## System Design

i am using nestjs cqrs pattern for build event driven application design.
Example image:

for more links :
- https://martinfowler.com/bliki/CQRS.html
- https://docs.nestjs.com/recipes/cqrs
- https://martinfowler.com/eaaDev/EventNarrative.html
![cqrs example image](https://camo.githubusercontent.com/bc31836cd3a0e7b8d1f85a23c3946cce86e7a59189f6ad333b242b7a264e1fe3/68747470733a2f2f63646e2d696d616765732d312e6d656469756d2e636f6d2f6d61782f313230302f312a66654d5f2d5648684b3637304c6c4551656b65734b672e706e67
)

### this application have four modules
- **`users`** : User resource via CQRS
- **`todos`** : Todo Resource via CQRS
- **`webhooks`** : users can be subscribe multiple events and when events triggered every listeners added to queques list and running one by one or multiple so its asynchronous and we are avoiding the overload 
- **`auditlogs`** : every write operation (command) or events are logged asynchronously and user can be accessed from ui with many filter operations

#### infra
 `mongodb` : is saving all information about this application

 `redis` : i am using redis for queue, when application scale to multiple instance it will be sure consistent and avoid one queue item run twice

### Postman Documentation

https://documenter.getpostman.com/view/3360430/UVsJw6dS#23cb5520-083c-4f65-a42d-4c5cdff8153b

### frontend application about this project

ui repo : https://gitlab.com/atilla59/event-driven-todo-ui

logs ui is single page application written in VueJs. this service using api for multiple filters
### demo for ui app
![alt text](./docs/example-gif-ui.gif)



## License

Nest is [MIT licensed](LICENSE).

##Todo
- use Helmet
- webhook queue could be seperated to each single url 
- make sure given parameter exist
- maybe using saga ,event store etc.
